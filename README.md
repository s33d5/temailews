This is a service or console application (pick your flavour) that connects domains to allow Microsoft Teams meetings to function when your primary domain is different from your meeting room domain.

This is intended for corporate environments where you have a meeting room that has its own Teams account.
