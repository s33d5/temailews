﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class EmailConnection
{
    public EmailConnection(string comEmail, string caEmail, string comPassword)
    {
        this.caEmail = caEmail;
        this.comEmail = comEmail;
        this.comPassword = comPassword;
    }
    public string comEmail;
    public string caEmail;
    public string comPassword;
}