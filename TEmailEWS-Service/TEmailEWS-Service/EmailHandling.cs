﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.WebServices.Data;

static class EmailHandling
{
    public static bool verbose = false;
    static int waitTime = 2 * 1000;
    public const string version = "v1.0.5-Final-ByeFromCarlLlewellyn"; //this is logged at the start of each run of the service, so I change this each time I make an update so that I know that I am running the correct version

    public static async System.Threading.Tasks.Task newMailThreadAsync(string baseEmail, string fwdEmail, string comPw)
    {
        Program.log.Info("Creating thread..");

        await System.Threading.Tasks.Task.Run(() =>
        {
            if (bool.TryParse(FileManagement.getVariable(FileManagement.carlVar), out bool result))
            {
                if (!result)
                    Program.log.Info($"Are you trying to disable Carl messages? Well, they can't be disabled... PS it's been {DateTime.Now - new DateTime(2022, 6, 14, 14, 30, 00)} hours since he left.");
            }

            bool run = true;
            EmailMessage tempEmailStore = null; //this is for any deletes in exception try catch
            ExchangeService service;
            List<Appointment> conflictingNonCancelledAppts = new List<Appointment>();
            while (run)
            {
                System.Threading.Thread.Sleep(waitTime);//wait to scan again so that we don't destroy the server

                service = new ExchangeService(ExchangeVersion.Exchange2016)//make sure to put the exchange version here or things like hashtags and @someone will cause an exception in meeting requests
                {
                    Credentials = new WebCredentials(baseEmail, comPw)
                };

                // This is the office365 webservice URL
                service.Url = new Uri("https://summer.gov.bc.ca/EWS/Exchange.asmx");

                try
                {
                    foreach (EmailMessage email in service.FindItems(WellKnownFolderName.Inbox, new ItemView(2)))
                    {
                        Program.log.Info("Loading email resource for: " + baseEmail);
                        email.Load();//always load anything from exchange before using it
                        tempEmailStore = email;//used later to delete in an exception

                        //if we have seen this email before, then it may be in a loop
                        //this is already checked and deleted in the exceptions, but it's done here incase something goes wrong in the exception handler
                        if (EmailErrorHandling.addEmailItem(email.Id.UniqueId) >= 11)//give it 11 chances to send - however this is deleted after 10 attempts in the sendErrorEmail()
                        {
                            Program.log.Info("This email: " + baseEmail + ", meeting:" + email.Subject + " - has had an error too many times, deleting..." + '\n');
                            email.Delete(DeleteMode.MoveToDeletedItems);
                            break;
                        }

                        if (email.Schema is MeetingRequestSchema)
                        {
                            Program.log.Info(baseEmail + " - Found meeting: " + email.Subject);

                            //get meeting item
                            MeetingRequest appointment = MeetingRequest.Bind(service, email.Id, new PropertySet(ItemSchema.MimeContent, ItemSchema.Subject));
                            //get the mime content here because for some reason it shits itself if you do it after a load
                            var mimeCont = appointment.MimeContent;
                            //load the appointment - this is needed for all EWS items
                            appointment.Load();

                            Program.log.Info("Checking conflicts for: " + baseEmail + ", meeting:" + email.Subject);

                            //have to load all conflicts
                            conflictingNonCancelledAppts.Clear();
                            foreach (var appts in appointment.ConflictingMeetings ?? Enumerable.Empty<Appointment>())
                            {
                                appts.Load();
                                if (!appts.IsCancelled)
                                    conflictingNonCancelledAppts.Add(appts);
                            }

                            if (conflictingNonCancelledAppts.Count() <= 0) //if there is no conflict
                            {
                                Program.log.Info("Hash table good for: " + baseEmail + ", meeting:" + email.Subject + " - Checking if meeting is cancelled...");

                                if (!appointment.IsCancelled)
                                {
                                    EmailMessage message = new EmailMessage(service);
                                    message.MimeContent = extractInviteMime(mimeCont); //this is needed to fix teams not recognising some meetings
                                    message.ToRecipients.Clear();//clear recips so that forwards aren't sent too original email forward recipients
                                    message.CcRecipients.Clear();
                                    message.ToRecipients.Add(fwdEmail); //forward to teams mailbox
                                    message.SendAndSaveCopy();
                                    Program.log.Info("Forwarded: " + baseEmail + ", meeting:" + email.Subject + " - Accepting meeting..." + '\n');
                                    appointment.Accept(true);//put accept last, this way the user will only get a meeting accepf if everything has worked
                                }
                                else
                                {
                                    Program.log.Info("Cannot sent non-conflict email: " + baseEmail + ", meeting:" + email.Subject + " - is cancelled, deleting meeting item...");
                                    email.Delete(DeleteMode.MoveToDeletedItems);//if it's a cancelled meeting, then just delete it as the appointment will update itself over exchange
                                }

                            }
                            else //send reject and reason if there is a conflict
                            {
                                Program.log.Info("Conflict found for: " + baseEmail + ", meeting:" + email.Subject + " - Sending decline" + '\n');

                                if (!appointment.IsCancelled)//need to check if the appt is declined - it will throw an exception if it tries to decline a cancelled meeting
                                {
                                    //send declined email with conflict data back to user
                                    EmailMessage message = new EmailMessage(service);
                                    message.Subject = "Meeting Declined: " + appointment.Subject;
                                    message.ToRecipients.Add(email.From);
                                    message.Body = $"Your meeting: {appointment.Subject} was DECLINED due to the following conflict(s): <br>{getConflictString(conflictingNonCancelledAppts)}";
                                    message.Send();
                                    appointment.Decline(false);//send decline with no response - we're making our own response email above
                                }
                                else
                                {
                                    Program.log.Info(baseEmail + ", meeting:" + email.Subject + " - is cancelled, deleting meeting item...");
                                    email.Delete(DeleteMode.MoveToDeletedItems);//if it's a cancelled meeting, then just delete it as the appointment will update itself over exchange
                                }
                            }
                        }
                        else //this is if the item is NOT a meeting item, so a reply, etc.
                        {
                            Program.log.Info(baseEmail + " - Not meeting item, deleting: " + email.Subject + '\n'); //if it's just a random email sent, or an update, just delete it
                            email.Delete(DeleteMode.MoveToDeletedItems);
                        }
                    }
                }
                catch (Exception e)
                {
                    sendErrorEmail(baseEmail, e, tempEmailStore, service);
                }
            }
        });
    }

    //return all conflicts as a string
    static string getConflictString(List<Appointment> appointments)
    {
        string apptString = "";
        foreach (var appts in appointments)
        {
            appts.Load();
            apptString += $"<br>{appts.Subject} <br> which starts: {appts.Start} and ends: {appts.End} <br>";
        }
        return apptString;
    }

    static void sendErrorEmail(string baseEmail, Exception e, EmailMessage tempEmailStore, ExchangeService service)
    {
        Program.log.Error("An error has occured in thread for: " + baseEmail + " Error:" + e.Message);
        try
        {
            if (!(e.InnerException is System.Net.WebException) && !(e.InnerException is System.IO.IOException) && !(e.InnerException is System.Net.Sockets.SocketException)) //if this is not just a connection issue
            {
                if (EmailErrorHandling.getCount(tempEmailStore.Id.UniqueId) == 10)//Only send an error email if this is the 10th time it has failed
                {
                    if (FileManagement.getVariable(FileManagement.errorEmailVar) != "")
                    {
                        //if the email is not null then we have retrieved an email, so send error, otherwise just send the error
                        if (tempEmailStore != null)
                        {
                            //send error email 
                            var em = new EmailMessage(service);
                            em.ToRecipients.Add(FileManagement.getVariable(FileManagement.errorEmailVar));
                            em.Subject = "TEmailEWS" + version + " - An error has occured 10 times in thread for: " + baseEmail + ". This item will now be deleted.";
                            em.Body = new MessageBody(BodyType.Text, "An error has occured 10 times in thread for: " + baseEmail + ". Email subject: " + tempEmailStore.Subject +
                                "\r\n\n Exception message: " + e.Message + "\r\n\n Stack trace: \r\n" + e.StackTrace + "\r\n\n Exception target site: " + e.TargetSite +
                                "\r\n\n Inner exception:" + e.InnerException + "\r\n\n Exception:\r\n" + e +
                                "\r\n\n Email unique ID: " + tempEmailStore.Id.UniqueId + "\r\n\n Anyone miss Carl? I do. It's been exactly: " + (DateTime.Now - new DateTime(2022, 6, 14, 14, 30, 00)) + " hours since he left. Really miss that guy."
                                +"\r\n\n Hash table dump: " + EmailErrorHandling.getHashDump());
                            em.Send();
                        }
                        else
                        {
                            var em = new EmailMessage(service);
                            em.ToRecipients.Add(FileManagement.getVariable(FileManagement.errorEmailVar));
                            em.Subject = "TEmailEWS" + version + " - -- EMAIL IS NULL -- An error has occured 10 times in thread for: " + baseEmail + ". This item will now be deleted."; ;
                            em.Body = new MessageBody(BodyType.Text, "An error has occured 10 times in thread for: " + baseEmail + "." +
                                "\r\n\n Exception message: " + e.Message + "\r\n\n Stack trace: \r\n" + e.StackTrace + "\r\n\n Exception target site: " + e.TargetSite +
                                "\r\n\n Inner exception:" + e.InnerException + "\r\n\n Exception:\r\n" + e + "\r\n\n Anyone miss Carl? I do. It's been exactly: " + (DateTime.Now - new DateTime(2022, 6, 14, 14, 30, 00)) + " hours since he left. Really miss that guy."
                                + "\r\n\n Hash table dump: " + EmailErrorHandling.getHashDump());
                            em.Send();
                        }
                    }
                    //delete the email as it clearly doesn't want to work for whatever reason
                    Program.log.Info("This email: " + baseEmail + ", meeting:" + tempEmailStore.Subject + " - has had an error too many times, deleting..." + '\n');
                    tempEmailStore.Delete(DeleteMode.MoveToDeletedItems);
                }
            }
        }
        catch (Exception a)//catch an exception if error email can't be handled
        {
            Program.log.Error("Cannot send error email from: " + baseEmail + " Error:" + a.Message + '\n');

            try
            {
                if (tempEmailStore != null)//this is a reference to the current email we've been looking at, the original is out of scope cos it's in a for loop
                {
                    EmailErrorHandling.resetEmail(tempEmailStore.Id.UniqueId); //if we can't send an error email, then we need to reset this otherwise the error email will never be sent
                }
            }
            catch (Exception x)
            {
                Program.log.Error("Cannot reset email after error email attempt, from: " + baseEmail + " Error:" + x.Message + '\n');
            }
        }
    }

    //Here I've just scanned the mime for all emails to only include the invite, because
    //there is a weird error where only SOME users cannot book meetings cos of an image that they have in their signature
    //this is only apparent when you see the receiving mailbox with teams, where teams ignores the meeting if it has the image for some reason
    static MimeContent extractInviteMime(MimeContent mimeContent)
    {
        string completeString = Encoding.UTF8.GetString(mimeContent.Content);

        List<string> newLineString = completeString.Split('\n').ToList();
        List<string> calBlock = new List<string>();
        List<string> htmlBlock = new List<string>();
        List<string> combBlocks = new List<string>();

        bool calendarBlockStartFound = false;
        string calendarBlockStartString = "";

        int currLineNo = 0;

        string newStringCont = "";

        //get the location of the html tag
        foreach (var line in newLineString)
        {
            htmlBlock.Add(line);
            if (line.Contains("</html>"))
            {
                break;
            }
            currLineNo++;
        }

        currLineNo = 0;

        //get the location of the calendar block start and end
        foreach (var line in newLineString)
        {
            if (!calendarBlockStartFound)
            {
                if (line.Contains("Content-Type: text/calendar; charset=\"utf-8\"; method=REQUEST"))
                {
                    //copy start and this line as it will be skipped
                    calendarBlockStartString = newLineString[currLineNo - 1].Replace('\r', ' ').Replace(" ", "");
                    calBlock.Add(newLineString[currLineNo - 1]);
                    calBlock.Add(line);
                    calendarBlockStartFound = true;
                    Program.log.Info("Found: calendar start block: " + calendarBlockStartString);
                }
            }
            else
            {
                calBlock.Add(line);
                if (line.Contains(calendarBlockStartString))
                {
                    Program.log.Info("Found: calendar end block: " + line);
                    break;
                }
            }
            currLineNo++;
        }

        //Combine html and cal block
        combBlocks = htmlBlock.Concat(calBlock).ToList();

        //convert back to a normal string
        foreach (var line in combBlocks)
        {
            newStringCont += line + '\n';
        }

        if (bool.TryParse(FileManagement.getVariable(FileManagement.mimeFlagVar), out bool result))
        {
            if (result)
                Program.log.Info("Returning mime content: " + newStringCont);
        }

        return new MimeContent("UTF-8", Encoding.UTF8.GetBytes(newStringCont));
    }
}