﻿using System;
using System.Collections.Generic;
using System.IO;

static class FileManagement
{
    static string emailsFileLocation = Directory.GetCurrentDirectory() + "\\" + "emails.txt";
    static readonly string defaultPassword = "dnfew/vP7p";//read only stores the password as bytes instead of text
    const string defaulePwVariable = "$defaultPassword";
    static string settingsFileLoc = Directory.GetCurrentDirectory() + "\\" + "settings.txt";

    //add any setting you want here
    public const string mimeFlagVar = "MIME_FLAG:";
    public const string errorEmailVar = "ERROR_EMAIL_ADDRESS:";
    public const string hashTableVar = "HASH_TABLE_LOG:";
    public const string sslCertVar = "IGNORE_SSL_CERTS:";
    public const string carlVar = "CARL_MESSAGES:";

    //these are their default values
    const bool mimeFlagDefault = false;
    const bool hashTableFlagDefault = false;
    const bool SSLCertBypassFlagDefault = false;
    const string defaultErrorEmailAdd = "itworkmo@victoria1.gov.bc.ca";
    const bool carlVarFlagDefault = true;

    //this will manage all the variables, where you give them the values set above
    public static VariableManagement variableManagement { get; private set; }

    static FileManagement()
    {
        variableManagement = new VariableManagement();
        //add each variable and its default to the variable manager
        variableManagement.addNewVariable(mimeFlagVar, mimeFlagDefault);
        variableManagement.addNewVariable(errorEmailVar, defaultErrorEmailAdd);
        variableManagement.addNewVariable(hashTableVar, hashTableFlagDefault);
        variableManagement.addNewVariable(sslCertVar, SSLCertBypassFlagDefault);
        variableManagement.addNewVariable(carlVar, carlVarFlagDefault);
    }

    public static string getVariable(string variable)
    {
        return variableManagement.getVariableSettingAsString(variable);
    }

    //get the settings from the settings.txt
    static public void getSettings()
    {
        try
        {
            if (File.Exists(settingsFileLoc))
            {
                bool aVarHasBeenSet = false; //this will be used to warn the user if a setting entered is invalid
                Program.log.Info("Found settings.txt.");
                foreach (var line in File.ReadAllLines(settingsFileLoc))
                {
                    if (!line.Contains(@"//"))//anything that has this in the line is a comment
                    {
                        aVarHasBeenSet = false;
                        //loop through all variables in the management list
                        foreach (var variable in variableManagement.settingVariables)
                        {
                            //if this line in the settings.txt matches something in the list, then we have found a setting to set
                            if (line.Contains(variable.variable))
                            {
                                //set the variable to whatever is found after the setting variable - validity is checked later
                                variableManagement.setVariable(variable.variable, getSettingFromLine(variable.variable, line));
                                //this is a valid setting so set this to true
                                aVarHasBeenSet = true;
                                break;
                            }
                        }

                        //if this hasn't been set then we know it isn't a comment or a valid setting
                        if(aVarHasBeenSet == false)
                        {
                            Program.log.Error(line + " is not a valid setting.");
                        }
                    }
                }
                variableManagement.checkVariablesSet();
            }
            else
            {
                Program.log.Error("Could not find settings.txt, all values will be set to their defaults.");
            }
        }
        catch (Exception e)
        {
            Program.log.Error("Could not load settings.txt, all values will be set to their defaults. Error: " + e.Message);
        }

    }

    //get the setting from the entered string, which just extracts the string from the end of the variable to the end of the line
    static string getSettingFromLine(string variable, string line)
    {
        return line.Substring(line.IndexOf(variable) + variable.Length, line.Length - variable.Length);
    }

    //get the email addresses and set them
    //this works in multiples of three for each email forwarding setup
    //The first thing found that isn't a comment is line 0
    //0 = the email address to forward from (.com/victoria1)
    //1 = the email address to forward to (.ca)
    //2 = the password for the forward from (the one at 0 above)
    static public List<EmailConnection> getEmails()
    {
        int currInnerLineNum = 0;
        List<EmailConnection> emailDetails = new List<EmailConnection>();
        string tempCaEmail = "";
        string tempComEmail = "";
        string tempPw = "";
        try
        {
            if (File.Exists(emailsFileLocation))
            {
                Program.log.Info("Found emails.txt.");
                foreach (var line in File.ReadAllLines(emailsFileLocation))
                {
                    if (!line.Contains(@"//")) //this is for comments
                    {
                        switch (currInnerLineNum)
                        {
                            case 0: 
                                tempComEmail = line;//0 = the email address to forward from (.com/victoria1)
                                break;
                            case 1:
                                tempCaEmail = line;//1 = the email address to forward to (.ca)
                                break;
                            case 2://2 = the password for the forward from (the one at 0 above)
                                if (line.ToLower() == defaulePwVariable.ToLower()) //if this is set to the default password variable, then just copy that in
                                {
                                    tempPw = defaultPassword; 
                                }
                                else
                                {
                                    tempPw = line;
                                }
                                //add this to the emails we are gonna use
                                emailDetails.Add(new EmailConnection(tempComEmail, tempCaEmail, tempPw));
                                currInnerLineNum = -1;//set to -1 so that the next loop starts at 0 (see the ++ below)
                                break;
                        }
                        currInnerLineNum++;
                    }
                }
            }
            else
            {
                Program.log.Error("Could not find emails.txt.");
            }
        }
        catch (Exception e)
        {
            Program.log.Error("Error getting emails from file: " + e.Message);
        }
        return emailDetails;//return all the email connections
    }
}