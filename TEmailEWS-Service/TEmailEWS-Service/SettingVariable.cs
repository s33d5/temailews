﻿/// <summary>
/// This is the basic variables and their settings
/// which stores the variable name, the default setting, and the setting it's set to
/// </summary>
class SettingVariable
{
    public string variable { get; private set; }//the variable name
    public string setting;//the setting the variable is set to
    string stringDefaultSetting;//the default setting if it is a non bool
    bool? boolDefaultSetting;//the default setting if the variable is a bool? - the question mark means it is nullable

    //2 overload constructors based on bool or string type
    public SettingVariable(string variable, string defaultSetting)
    {
        this.variable = variable;
        this.stringDefaultSetting = defaultSetting;
    }

    public SettingVariable(string variable, bool defaultSetting)
    {
        this.variable = variable;
        this.boolDefaultSetting = defaultSetting;
    }

    //get the defaule setting for this variable, return as a string
    public string getDefaultSettingAsString()
    {
        if(boolDefaultSetting.HasValue) //if the boolean has been set
        {
            return boolDefaultSetting.ToString();
        }else if(stringDefaultSetting != null)//if not then this must be a string
        {
            return stringDefaultSetting;
        }
        return null;//if it's not set at all return null so caller knows
    }
}