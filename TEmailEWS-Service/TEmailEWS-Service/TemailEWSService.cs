﻿using System.Collections.Generic;
using System.ServiceProcess;
using System.Net;
using System;
public partial class TemailEWSService : ServiceBase
{
    static int noOfThreads = 0;
    static List<EmailConnection> emailConnections = new List<EmailConnection>();

    public TemailEWSService()
    {
        InitializeComponent();
    }

    protected override void OnStart(string[] args)
    {
        Program.log.Info("Service started.");
        setupThreads();
    }

    protected override void OnStop()
    {
        Program.log.Info("Service stopped.");
    }
    static void setupThreads()
    {
        try
        {
            Program.log.Info("Starting TEmail " + EmailHandling.version);
            bool run = true;
            emailConnections = FileManagement.getEmails();
            //NetworkMessage nwMsg = new NetworkMessage();
            //nwMsg.recieve(); Enable this if want to use messages over the network
            FileManagement.getSettings();

            if (bool.TryParse(FileManagement.getVariable(FileManagement.carlVar), out bool carlVarResult))
            {
                if (carlVarResult)
                    Program.log.Info($"How long has it been since Carl left again? Oh yeah, exactly: {DateTime.Now - new DateTime(2022, 6, 14, 14, 30, 00)} hours. Miss that guy.");
            }

            if (bool.TryParse(FileManagement.getVariable(FileManagement.sslCertVar), out bool result))
            {
                if (result)
                {
                    Program.log.Info("Turning off SSL checks...");
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; }; //turn ssl checks if if we need to
                }
            }

            EmailErrorHandling.newResetHashMidnight();//start thread for the hash table reset
           
            foreach (var eConn in emailConnections)
            {
                noOfThreads++;
                Program.log.Info("INFO: Creating procesing thread for: " + eConn.comEmail + " which forwards to: " + eConn.caEmail);
                EmailHandling.newMailThreadAsync(eConn.comEmail, eConn.caEmail, eConn.comPassword);
            }

            Program.log.Info(noOfThreads + " threads created succefully.");
        }
        catch (System.Exception e)
        {
            Program.log.Error("Something went wrong creating threads: " + e.Message);
        }
    }
}
