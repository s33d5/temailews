﻿using System.Collections.Generic;
using System.Collections;

/// <summary>
/// Class to manage all variables using a list and a hashtable
/// </summary>
class VariableManagement
{
    //a list of SettngVarialbes which contain the setting, the variable name, and the default variable
    public List<SettingVariable> settingVariables { get; private set; }
    //this is a hashtable that stores the element number in the list of the variables, so we can easily retrieve and set variables
    Hashtable allSettingsListPositions = new Hashtable();

    public VariableManagement()
    {
        settingVariables = new List<SettingVariable>();
    }

    //2 overload methods, one with bool
    //this will add a new SettingVariable to the list
    //it will also store in the hashtable what element number in the list this is
    public void addNewVariable(string variable, string defaultSetting)
    {
        settingVariables.Add(new SettingVariable(variable, defaultSetting));
        allSettingsListPositions.Add(variable, settingVariables.Count - 1);
    }

    public void addNewVariable(string variable, bool defaultSetting)
    {
        settingVariables.Add(new SettingVariable(variable, defaultSetting));
        allSettingsListPositions.Add(variable, settingVariables.Count - 1);
    }

    //set the variable based on the variable name
    //if it isn't a valid variable then it wont be set and therefore left as null
    public void setVariable(string variable, string setting)
    {
        //check if the default is a bool
        //we are parsing the hash table's stored value to get the list element
        if (bool.TryParse(settingVariables[int.Parse(allSettingsListPositions[variable].ToString())].getDefaultSettingAsString(), out bool result))
        {
            //if it is then see if the set setting is correct
            if(bool.TryParse(setting, out bool secondResult))
            {
                //if it is then set it
                settingVariables[int.Parse(allSettingsListPositions[variable].ToString())].setting = secondResult.ToString();
            }
            else
            {
                Program.log.Error(variable + " does not have a valid boolean, so it will be set to its default.");
            }
        }
        else
        {
            //if this isn't a bool then just store the string
            settingVariables[int.Parse(allSettingsListPositions[variable].ToString())].setting = setting;
        }
    }

    //this will return a variable as a string - this is done so that we can just parse the values on return and this class can support multiple types
    public string getVariableSettingAsString(string variable)
    {
        return settingVariables[int.Parse(allSettingsListPositions[variable].ToString())].setting;
    }

    //check if all variables are set or not
    public void checkVariablesSet()
    {
        foreach(var settingVariable in settingVariables)
        {
            //if the setting is null then just set it to its default value
            if(settingVariable.setting == null)
            {
                Program.log.Warn(settingVariable.variable + " not found, setting to default: " + settingVariable.getDefaultSettingAsString());
                settingVariable.setting = settingVariable.getDefaultSettingAsString();
            }
            else
            {
                Program.log.Info(settingVariable.variable + " found and set to: " + settingVariable.setting);
            }
        }
    }

}